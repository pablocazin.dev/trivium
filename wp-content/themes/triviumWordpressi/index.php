<?php
    $pageDescription = "bienvenue";
    include('header.php');
?>
<?php $wpb_all_query = new WP_Query(array('post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => -1, 'orderby' => 'title', 'order'   => 'ASC',));
?>
<?php
if (have_posts()) :
    $previousLetter = ""; ?>

    <div class="row">
        <?php while ($wpb_all_query->have_posts()) : $wpb_all_query->the_post(); 
            $wpb_all_query->get_the_item_object ?>
            <?php
                $nom = get_the_title();
                $lettre = substr($nom, 0, 1);
                if ( strcmp($previousLetter, $lettre) !== 0) {
                    $previousLetter = $lettre;
                    echo "<h2>".$lettre."</h2>";
                }
            ?>
            <div class="col-sm-4 text-center animal-card">
                <div class="card" style="width: 18rem;">
                    <?php the_post_thumbnail(
                        'medium',
                        [
                            'class' => 'card-img-top',
                            'alt' => get_the_title()
                        ]
                    );
                    ?>
                    <?php $title = get_the_title();
                    //echo substr($title, 0, 1); ?>
                    <div class="card-body">
                        <h5 class="card-title"><?php the_title(); ?></h5>
                        <?php if(get_the_excerpt()):?>
                            <p><?php echo the_excerpt(); ?></p>
                            <?php endif; ?>
                            <a href="<?php the_permalink(); ?>" class="btn btn-primary">voir plus</a>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
<?php else : ?>
    <h1>Pas d'articles</h1>
<?php endif; ?>
<?php get_footer() ?>