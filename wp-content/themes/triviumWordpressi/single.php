<?php
if (have_posts()) : 
    $pageDescription = " - vous etes sur la page " . get_the_title();
    include('header.php');?>
    <div class="">
        <?php while (have_posts()) : the_post(); ?>
            <div class="single-container">
                <?php the_post_thumbnail(
                    'medium',
                    [
                        'class' => 'single-img',
                        'alt' => get_the_title()
                    ]
                );
                ?>
                <div class="single-content">
                    <h1 class=""><?php the_title(); ?></h1>
                    <?php if (get_field('taille')) : ?>
                        <p>taille : <?php the_field('taille'); ?> toise(s)</p>
                    <?php endif; ?>
                    <p class=""> Description : <?php the_content(); ?></p>
    
                    <?php
                    $mange = get_field('mange');
                    if ($mange) : ?>
                        <ul>
                            <p><?php the_title(); ?> mange :</p>
                            <?php foreach ($mange as $animal) :
                                $permalink = get_permalink($animal->ID);
                                $title = get_the_title($animal->ID); 
                            ?>
                                <li>
                                    <a href="<?php echo esc_url($permalink); ?>"><?php echo esc_html($title); ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>

                </div>
            </div>
        <?php endwhile; ?>
    </div>
<?php else : ?>
    <h1>Erreur de chargement...</h1>
<?php endif; ?>
<?php get_footer() ?>